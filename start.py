from endpoints import (
    Serie as Class_serie,
    PrintException,
    adx_process,
    mkdir
)
import json
import sys
import os


def mainProcess(params=None, path=None):
    try:
        start_date = params['start_date']
        end_date = params['end_date']
        interval = params['interval']
        stocks = params['stocks']
        for stock_name in stocks:
            serie = Class_serie(symbol=stock_name)
            dataframe = serie.get_serie(
                start_date=start_date,
                end_date=end_date,
                interval=interval
            )
            dataframe = serie.process(serie=dataframe)
            dataframe_adx = adx_process(serie=dataframe)
            if dataframe_adx is not False:
                filename = os.path.join(
                    path,
                    '{}.csv'.format(stock_name.replace(
                        ".", '_')
                    )
                )
                dataframe.to_csv(filename, index=False)
    except Exception:
        PrintException()


if __name__ == '__main__':
    try:
        cwd = os.getcwd()
        folder_path = os.path.join(cwd, 'timeseries')
        f = open('conf_file.json')
        params = json.load(f)
        mkdir(directory="timeseries")
        mainProcess(params=params, path=folder_path)
    except Exception:
        PrintException()
        sys.exit("FINISHED")
