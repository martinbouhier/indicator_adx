# ADX Indicator

ADX Stock Indicator with Candlestick with Plotly and Dashboard.
![](https://drive.google.com/uc?export=view&id=10Fi3OVe2WKlqL-Rhm6I35vpEDqjZjZxK)

## Installation

First, you should create a Virtual Enviroment
```bash
python3 -m venv ADX
```
Activate venv and clone the repository
```bash
cd ADX
source bin/activate
git clone https://www.gitlab.com/martinbouhier/indicator_adx
cd indicator_adx
pip install -r requirements.txt
```

## Usage
Edit conf_file to change params
```bash
{
    "start_date": "2020-01-01",
    "end_date": "2020-07-24",
    "interval": "d",
    "stocks": ["BTC-USD", "ETH-USD", "XRP-USD", "ADA-USD"]
}

```
Now, run the `start.py` script and then visualized with `dashboard.py`
```bash
python start.py
python dashboard.py
```

## Author
Martin Bouhier [linkedin](https://www.linkedin.com/in/martinbouhier)

Email: martinbouhier@outlook.com

