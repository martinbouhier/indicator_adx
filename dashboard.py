# DASHBOARD
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import glob
import os
from endpoints import Plots


def mainProcess():
    figures = {}
    for item in data:
        ticker = item.split('/')[-1].replace('.csv', '')
        dataframe = pd.read_csv(item)
        ploting = Plots(symbol=ticker, serie=dataframe)
        figure = ploting.fig(
            rows=2, cols=1, dragmode='pan', width=1200, height=600,
            autosize=False, xaxis_rangeslider_visible=False,
            x_title='Dates', y_title='Prices')
        ploting.candlestick(fig=figure, row=1, col=1)
        ploting.scatter(
            fig=figure, mode='lines', name='ADX', opacity=1, color='blue',
            width=4, row=2, col=1, x_col='index', y_col='adx'
        )
        ploting.scatter(
            fig=figure, mode='lines', name='DI+', opacity=1, color='green',
            width=1, row=2, col=1, x_col='index', y_col='DI+'
        )
        ploting.scatter(
            fig=figure, mode='lines', name='DI-', opacity=1, color='red',
            width=1, row=2, col=1, x_col='index', y_col='DI-'
        )
        ploting.scatter(
            fig=figure, mode='lines', name='25 %', opacity=1,
            color='black', width=1, row=2, col=1,
            x_col=(dataframe.iloc[0].name, dataframe.iloc[-1].name),
            y_col=(25, 25)
        )

        figures[ticker] = figure

    fig_names = list(figures.keys())

    return figures, fig_names


if __name__ == '__main__':
    cwd = os.getcwd()
    folder_path = os.path.join(cwd, 'timeseries')
    data = glob.glob("{}/*.csv".format(folder_path))
    figures, fig_names = mainProcess()
    app = dash.Dash()
    fig_dropdown = html.Div([
        dcc.Dropdown(
            id='fig_dropdown',
            options=[{'label': x, 'value': x} for x in fig_names],
            value=fig_names[0]
        )])
    fig_plot = html.Div(id='fig_plot')

    app.layout = html.Div([fig_dropdown, fig_plot])

    @app.callback(
        dash.dependencies.Output('fig_plot', 'children'),
        [dash.dependencies.Input('fig_dropdown', 'value')]
    )
    def update_output(fig_name):
        return name_to_figure(fig_name)

    def name_to_figure(fig_name):
        return dcc.Graph(figure=figures[fig_name], config={'scrollZoom': True})

    app.run_server(debug=True, use_reloader=False)
