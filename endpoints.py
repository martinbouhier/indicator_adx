from plotly.subplots import make_subplots
import pandas_datareader.data as web
import plotly.graph_objects as go
from ta.trend import ADXIndicator
import linecache
import shutil
import sys
import os


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


class Serie(object):

    def __init__(self, symbol=None):
        super(Serie, self).__init__()
        self.symbol = symbol

    def get_serie(self, start_date=None, end_date=None, interval=None):
        # Get data from Web
        stock = self.symbol
        try:
            serie = web.get_data_yahoo(
                stock, start_date,
                end_date, interval=interval
            )
            return serie
        except Exception:
            PrintException()
            return False

    def process(self, serie=None):
        try:
            # Eliminamos los NaN rows (NaN en invervalos mayores a 1d)
            serie = serie.dropna()
            # Convertimos las cabeceras de la serie a minusculas
            serie.columns = map(
                str.lower, serie.columns
            )
            # Nos quedamos solo con las siguientes columnas
            serie = serie[
                ['open', 'high', 'low', 'close', 'adj close', 'volume']
            ]
            # Insertamos la fecha que viene seteada en el index como una columna en la posicion 0
            # y reseteamos el index a valor numerico para realizar operaciones luego
            serie.insert(0, 'date', serie.index)
            serie = serie.reset_index(drop=True)
            serie['date'] = serie['date'].dt.strftime('%Y-%m-%d')
            return serie
        except Exception:
            PrintException()
            return False


def adx_process(serie=None):
    try:
        adxI = ADXIndicator(
            serie.high, serie.low,
            serie.close, 14, False
        )
        serie['DI+'] = adxI.adx_pos()
        serie['DI-'] = adxI.adx_neg()
        serie['adx'] = adxI.adx()
        return serie
    except Exception:
        PrintException()
        return False


def mkdir(directory=None):
    if not os.path.isdir(directory):
        oldmask = os.umask(000)
        os.makedirs(directory, 0o777)
        os.umask(oldmask)
    elif os.path.isdir(directory):
        shutil.rmtree(directory, ignore_errors=True)
        oldmask = os.umask(000)
        os.makedirs(directory, 0o777)
        os.umask(oldmask)


# PLOTING ENDPOINTS #
class Plots(object):

    def __init__(self, symbol=None, serie=None):
        super(Plots, self).__init__()
        self.symbol = symbol
        self.serie = serie

    def fig(
        self, width=None, height=None, autosize=None,
        xaxis_rangeslider_visible=False, rows=None, cols=None,
        x_title=None, y_title=None, dragmode=None
    ):
        symbol = self.symbol
        fig = make_subplots(rows=2, cols=1)
        fig.update_layout(dragmode='pan', title_text=symbol, width=1200, height=600, autosize=False, xaxis_rangeslider_visible=False)
        fig.update_xaxes(title_text="dates")
        fig.update_yaxes(title_text="price")
        return fig

    def candlestick(self, fig=None, row=None, col=None):
        serie = self.serie
        fig.add_trace(
            go.Candlestick(
                x=serie.index,
                open=serie.open,
                high=serie.high,
                low=serie.low,
                close=serie.close
            ),
            row=row, col=col
        )

    def scatter(
        self, fig=None, mode=None, name=None, opacity=None, color=None,
        width=None, row=None, col=None, x_col=None, y_col=None
    ):
        serie = self.serie
        if type(x_col) is tuple:
            x = x_col
        elif x_col == 'index':
            x = serie.index
        else:
            x = serie[x_col]
        if type(y_col) is tuple:
            y = y_col
        else:
            y = serie[y_col]

        fig.add_trace(
            go.Scatter(
                x=x, y=y,
                mode=mode, name=name,
                opacity=opacity,
                line=dict(
                    color=color,
                    width=width,
                )
            ),
            row=row, col=col
        )
